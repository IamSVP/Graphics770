#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>

#include <cstdio>
#include  <cstdlib>

#include <GL/glew.h>

#include <glfw3.h>
#include <limits>


GLFWwindow *window;
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <algorithm>
#include <vector>

 
#include <stdio.h>
#include <string.h>
#include <string>
#include <vector>
#include <queue>
#include <fstream>
#include <float.h>
#include <cassert>

#ifdef _MSC_VER

typedef __int16 int16;
typedef unsigned __int16 uint16;
typedef __int32 int32;
typedef unsigned __int32 uint32;
typedef __int8 int8;
typedef unsigned __int8 uint8;

typedef unsigned __int64 uint64;
typedef __int64 int64;

#include <tchar.h>
typedef TCHAR CHAR;
#endif



//#define VBO
#define IMMEDIATE

#ifdef DEBUG

static const char* getGLErrString(GLenum err){

    const char *errString = "Unknown error";
    switch(err){
        case GL_INVALID_ENUM: errString = "Invalid enum"; break;
        case GL_INVALID_VALUE: errString = "Invalid value"; break;
        case GL_INVALID_OPERATION: errString = "Invalid operation"; break;
        case GL_INVALID_FRAMEBUFFER_OPERATION: errString = "Invalid Frame Buffer Operation"; break;
        case GL_OUT_OF_MEMORY: errString = "Out of Memory Operation"; break;
    }
    return errString;
}




#define CHECK_GL(fn, ...) fn(__VA_ARGS__);                                           \
  do {                                                                               \
    GLenum glErr = glGetError();                                                     \
    if(glErr != GL_NO_ERROR) {                                                       \
      const char *errString = getGLErrString(glErr);                                 \
      if(NULL != errString){                                                         \
        printf("OpenGL call Error (%s : %d): %s\n",__FILE__, __LINE__, errString);    \
      } else {                                                                       \
        printf("Unknown OpenGL call Error(%s : %d): %s\n", __FILE__);                 \
       }                                                                             \
      assert(false);                                                                 \
    }                                                                                \
  } while(0)
#else
#define CHECK_GL(fn, ...) fn(__VA_ARGS__);
#endif


const char *vsSoruce = "#version 330 core\n"
"layout(location = 0) in vec3 vertexPosition_m;\n"
"layout(location = 1) in vec3 vertexNormal_m;\n"



"out vec3 Position_w;\n"
"out vec3 Normal_w;\n"


"uniform mat4 MVP;\n"
"uniform mat4 M;\n"
"uniform mat4 M_INV_TR;\n"

"void main() {\n"

"gl_Position = MVP * vec4(vertexPosition_m, 1);\n"

"Position_w = (M * vec4(vertexPosition_m, 1)).xyz;\n"

"Normal_w = (M_INV_TR * vec4(vertexNormal_m, 0)).xyz; \n"

"}\n";


const char *fsSource = "#version 330 core\n"

"in vec3 Position_w;\n"
" in vec3 Normal_w;\n"


"out vec3 color;\n"

"void main(){\n"

"vec3 LightColor = vec3(1,1,1);\n"
"vec3 l = normalize(vec3(1,1, 1));\n"



"vec3 n = normalize(Normal_w);\n"

"float cosTheta = clamp(dot(n,l),0,1);\n"

"vec3 Ambient = vec3(0.2,0.2,0.2);\n "
"vec3 Diffuse = vec3(1,1,1) * cosTheta; \n"

"color = Ambient + Diffuse;\n"

"}\n";






struct Vector3
{
	float			x, y, z;
};

struct Triangle
{
	unsigned int 	indices[3];
};

std::vector<Vector3>	gPositions;
std::vector<Vector3>	gNormals;
std::vector<Triangle>	gTriangles;

void tokenize(char* string, std::vector<std::string>& tokens, const char* delimiter)
{
	char* token = strtok(string, delimiter);
	while (token != NULL)
	{
		tokens.push_back(std::string(token));
		token = strtok(NULL, delimiter);
	}
}

int face_index(const char* string)
{
	int length = strlen(string);
	char* copy = new char[length + 1];
	memset(copy, 0, length + 1);
	strcpy(copy, string);

	std::vector<std::string> tokens;
	tokenize(copy, tokens, "/");
	delete[] copy;
	if (tokens.front().length() > 0 && tokens.back().length() > 0 && atoi(tokens.front().c_str()) == atoi(tokens.back().c_str()))
	{
		return atoi(tokens.front().c_str());
	}
	else
	{
		printf("ERROR: Bad face specifier!\n");
		exit(0);
	}
}

void load_mesh(std::string fileName)
{
	std::ifstream fin(fileName.c_str());
	if (!fin.is_open())
	{
		printf("ERROR: Unable to load mesh from %s!\n", fileName.c_str());
		exit(0);
	}

	float xmin = FLT_MAX;
	float xmax = -FLT_MAX;
	float ymin = FLT_MAX;
	float ymax = -FLT_MAX;
	float zmin = FLT_MAX;
	float zmax = -FLT_MAX;

	while (true)
	{
		char line[1024] = { 0 };
		fin.getline(line, 1024);

		if (fin.eof())
			break;

		if (strlen(line) <= 1)
			continue;

		std::vector<std::string> tokens;
		tokenize(line, tokens, " ");

		if (tokens[0] == "v")
		{
			float x = atof(tokens[1].c_str());
			float y = atof(tokens[2].c_str());
			float z = atof(tokens[3].c_str());

			xmin = std::min(x, xmin);
			xmax = std::max(x, xmax);
			ymin = std::min(y, ymin);
			ymax = std::max(y, ymax);
			zmin = std::min(z, zmin);
			zmax = std::max(z, zmax);

			Vector3 position = { x, y, z };
			gPositions.push_back(position);
		}
		else if (tokens[0] == "vn")
		{
			float x = atof(tokens[1].c_str());
			float y = atof(tokens[2].c_str());
			float z = atof(tokens[3].c_str());
			Vector3 normal = { x, y, z };
			gNormals.push_back(normal);
		}
		else if (tokens[0] == "f")
		{
			unsigned int a = face_index(tokens[1].c_str());
			unsigned int b = face_index(tokens[2].c_str());
			unsigned int c = face_index(tokens[3].c_str());
			Triangle triangle;
			triangle.indices[0] = a - 1;
			triangle.indices[1] = b - 1;
			triangle.indices[2] = c - 1;
			gTriangles.push_back(triangle);
		}
	}

	fin.close();

	printf("Loaded mesh from %s. (%lu vertices, %lu normals, %lu triangles)\n", fileName.c_str(), gPositions.size(), gNormals.size(), gTriangles.size());
	printf("Mesh bounding box is: (%0.4f, %0.4f, %0.4f) to (%0.4f, %0.4f, %0.4f)\n", xmin, ymin, zmin, xmax, ymax, zmax);
}



float  					gTotalTimeElapsed = 0;
int 					gTotalFrames = 0;
GLuint 					gTimer;






void init_timer()
{
	glGenQueries(1, &gTimer);
}

void start_timing()
{
	glBeginQuery(GL_TIME_ELAPSED, gTimer);
}

float stop_timing()
{
	glEndQuery(GL_TIME_ELAPSED);

	GLint available = GL_FALSE;
	while (available == GL_FALSE)
		glGetQueryObjectiv(gTimer, GL_QUERY_RESULT_AVAILABLE, &available);

	GLint result;
	glGetQueryObjectiv(gTimer, GL_QUERY_RESULT, &result);

	float timeElapsed = result / (1000.0f * 1000.0f * 1000.0f);
	return timeElapsed;
}


int main(int argc, char *argv[]) {

	if (!glfwInit()) 
	{
		fprintf(stderr, "failed to intialize GLFW\n");
		getchar();
		return -1;
	
	}
	

	//glfwWindowHint(GLFW_SAMPLES, 4);
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	//
	window = glfwCreateWindow(512, 512, "Bunny", NULL, NULL);
	if (window == NULL) {
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	CHECK_GL(glClearColor, 0.0f, 0.0f, 0.0, 0.0);
	
	CHECK_GL(glDepthFunc, GL_LESS);
load_mesh("bunny.obj");





init_timer();

#ifdef IMMEDIATE

do {

	start_timing();
	CHECK_GL(glEnable, GL_DEPTH_TEST);
	CHECK_GL(glClear, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	CHECK_GL(glMatrixMode, GL_MODELVIEW);
	
	CHECK_GL(glLoadIdentity);
	CHECK_GL(gluLookAt, 0, 0, 0, 0, 0, -1, 0 ,1, 0);
	CHECK_GL(glTranslatef, 0.1, -1, -1.5);
	CHECK_GL(glScalef, 10, 10, 10);
	
	CHECK_GL(glMatrixMode, GL_PROJECTION);
	CHECK_GL(glLoadIdentity);
	CHECK_GL(glFrustum, -0.1, 0.1, -0.1, 0.1, 0.1, 1000);
	CHECK_GL(glViewport, 0, 0, 512, 512);

	
	// shading properites 

	float ka[] = {1, 1, 1, 0};
	float kd[] = {1, 1, 1, 0};
	float ks[] = {0, 0, 0, 0};
	float p = 0;
	CHECK_GL(glEnable, GL_LIGHTING);
	CHECK_GL(glEnable, GL_LIGHT0);

	CHECK_GL(glMaterialfv, GL_FRONT, GL_DIFFUSE, kd);
	CHECK_GL(glMaterialfv, GL_FRONT, GL_AMBIENT, ka);
	CHECK_GL(glMaterialfv, GL_FRONT, GL_SPECULAR, ks);
	CHECK_GL(glMaterialf, GL_FRONT, GL_SHININESS, p);

	float Ia[] = {0.2, 0.2, 0.2, 0};
	float l[] = {1, 1, 1, 0};
	float la[] = {0, 0, 0, 0};
	float ld[] = {1, 1, 1, 0};
	float ls[] = {0, 0, 0, 0};

	CHECK_GL(glLightModelfv, GL_LIGHT_MODEL_AMBIENT, Ia);
	CHECK_GL(glLightfv, GL_LIGHT0, GL_POSITION, l);
	CHECK_GL(glLightfv, GL_LIGHT0, GL_AMBIENT, la);
	CHECK_GL(glLightfv, GL_LIGHT0, GL_DIFFUSE, ld);
	CHECK_GL(glLightfv, GL_LIGHT0, GL_SPECULAR, ls);
	//CHECK_GL(glShadeModel, GL_PHONG)
	

	CHECK_GL(glBegin, GL_TRIANGLES);
	for(int i = 0; i < gTriangles.size(); i++){


		int k0 = gTriangles[i].indices[0];
		int k1 = gTriangles[i].indices[1];
		int k2 = gTriangles[i].indices[2];	
		
		

		CHECK_GL(glNormal3f, gNormals[k0].x, gNormals[k0].y, gNormals[k0].z);

		CHECK_GL(glVertex3f, gPositions[k0].x, gPositions[k0].y, gPositions[k0].z);

		CHECK_GL(glNormal3f, gNormals[k1].x, gNormals[k1].y, gNormals[k1].z);
		CHECK_GL(glVertex3f,gPositions[k1].x, gPositions[k1].y, gPositions[k1].z );
		
		CHECK_GL(glNormal3f, gNormals[k2].x, gNormals[k2].y, gNormals[k2].z);
		CHECK_GL(glVertex3f,gPositions[k2].x, gPositions[k2].y, gPositions[k2].z );
		
		
			
					
	}	

	CHECK_GL(glEnd);
	float timeElapsed = stop_timing();
	gTotalFrames++;
	gTotalTimeElapsed += timeElapsed;
	float fps = gTotalFrames / gTotalTimeElapsed;
	char string2[1024] = { 0 };
	sprintf(string2, "OpenGL Bunny: %0.2f FPS", fps);
	glfwSetWindowTitle(window, string2);
	glfwSwapBuffers(window);
	glfwPollEvents();


	

	



} while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
		glfwWindowShouldClose(window) == 0);






#endif



#ifdef VBO
	GLuint VertexId;
	CHECK_GL(glGenVertexArrays, 1, &VertexId);
	CHECK_GL(glBindVertexArray, VertexId);

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	for (int i = 0; i < gTriangles.size(); i++) {
	
		int k0 = gTriangles[i].indices[0];
		int k1 = gTriangles[i].indices[1];
		int k2 = gTriangles[i].indices[2];

		vertices.push_back(glm::vec3(gPositions[k0].x, gPositions[k0].y, gPositions[k0].z));
		vertices.push_back(glm::vec3(gPositions[k1].x, gPositions[k1].y, gPositions[k1].z));
		vertices.push_back(glm::vec3(gPositions[k2].x, gPositions[k2].y, gPositions[k2].z));
		
		normals.push_back(glm::vec3(gNormals[k0].x, gNormals[k0].y, gNormals[k0].z));
		normals.push_back(glm::vec3(gNormals[k1].x, gNormals[k1].y, gNormals[k1].z));
		normals.push_back(glm::vec3(gNormals[k2].x, gNormals[k2].y, gNormals[k2].z));	
	
	}

	
	glm::mat4 Projection = glm::perspective(90.0f, 1.0f, 0.1f, 1000.0f);
	glm::mat4 View = glm::lookAt(glm::vec3(0,0,0), glm::vec3(0,0,-1), glm::vec3(0,1,0));
	glm::mat4 Model = glm::translate(glm::mat4(1.0f), glm::vec3(0.1f, -1.0f, -1.5f)) *glm::scale(glm::mat4(1.0f), glm::vec3(10.0f, 10.0f, 10.0f));
	glm::mat4 MVP = Projection * View * Model;
	glm::mat4 Mode_inv_tr = glm::inverse(glm::transpose(Model));
GLint Result = GL_FALSE;
	int InfoLogLength;


	GLuint vrShrID = glCreateShader(GL_VERTEX_SHADER);
	CHECK_GL(glShaderSource, vrShrID, 1, &vsSoruce, NULL);
	CHECK_GL(glCompileShader, vrShrID);
// Check vertex Shader, taken from a github opengl tutorials
	CHECK_GL(glGetShaderiv, vrShrID, GL_COMPILE_STATUS, &Result);
	CHECK_GL(glGetShaderiv, vrShrID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
		CHECK_GL(glGetShaderInfoLog, vrShrID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		printf("%s\n", &VertexShaderErrorMessage[0]);
	}


	GLuint frShrID = glCreateShader(GL_FRAGMENT_SHADER);
	CHECK_GL(glShaderSource, frShrID, 1, &fsSource, NULL);
	CHECK_GL(glCompileShader,frShrID);
	// Check Fragment Shader, taken from a github opengl tutorials
	CHECK_GL(glGetShaderiv, frShrID, GL_COMPILE_STATUS, &Result);
	CHECK_GL(glGetShaderiv, frShrID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
		CHECK_GL(glGetShaderInfoLog, frShrID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		printf("%s\n", &FragmentShaderErrorMessage[0]);
	}




	GLuint ProgramID = CHECK_GL(glCreateProgram); 
	CHECK_GL(glAttachShader, ProgramID, vrShrID);
	CHECK_GL(glAttachShader,ProgramID, frShrID);
	CHECK_GL(glLinkProgram, ProgramID);

	CHECK_GL(glUseProgram, ProgramID);

	GLuint MatrixID = CHECK_GL(glGetUniformLocation,ProgramID, "MVP");
	GLuint ModelID = CHECK_GL(glGetUniformLocation, ProgramID, "M");
	GLuint ModelINVID = CHECK_GL(glGetUniformLocation, ProgramID, "M_INV_TR");

	GLuint VertexBuffer, NormalBuffer;

	CHECK_GL(glGenBuffers, 1, &VertexBuffer); 
	CHECK_GL(glGenBuffers, 1, &NormalBuffer);

	CHECK_GL(glBindBuffer,GL_ARRAY_BUFFER, VertexBuffer);
	CHECK_GL(glBufferData, GL_ARRAY_BUFFER, vertices.size()*sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	CHECK_GL(glBindBuffer, GL_ARRAY_BUFFER, NormalBuffer);
	CHECK_GL(glBufferData, GL_ARRAY_BUFFER, normals.size()*sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);

	CHECK_GL(glEnable, GL_DEPTH_TEST);
	do {

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		CHECK_GL(glUseProgram, ProgramID);

		CHECK_GL(glUniformMatrix4fv, MatrixID, 1, GL_FALSE, &MVP[0][0]);
		CHECK_GL(glUniformMatrix4fv, ModelID,1, GL_FALSE, &Model[0][0]);
		CHECK_GL(glUniformMatrix4fv, ModelINVID, 1, GL_FALSE, &Mode_inv_tr[0][0]);

		CHECK_GL(glEnableVertexAttribArray, 0);
		CHECK_GL(glBindBuffer, GL_ARRAY_BUFFER, VertexBuffer);
		CHECK_GL(glVertexAttribPointer, 0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		CHECK_GL(glEnableVertexAttribArray, 1);
		CHECK_GL(glBindBuffer, GL_ARRAY_BUFFER, NormalBuffer);
		CHECK_GL(glVertexAttribPointer, 1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
		start_timing();
		CHECK_GL(glDrawArrays, GL_TRIANGLES, 0, vertices.size());
		float timeElapsed = stop_timing();
  	gTotalFrames++;
  	gTotalTimeElapsed += timeElapsed;
  	float fps = gTotalFrames / gTotalTimeElapsed;
  	char string2[1024] = {0};
  	sprintf(string2, "OpenGL Bunny: %0.2f FPS", fps);
  	glfwSetWindowTitle(window, string2);

		CHECK_GL(glDisableVertexAttribArray, 0);
		CHECK_GL(glDisableVertexAttribArray, 1);

		glfwSwapBuffers(window);
		glfwPollEvents();


	} while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
		glfwWindowShouldClose(window) == 0);

	CHECK_GL(glDeleteBuffers, 1, &VertexBuffer);
	CHECK_GL(glDeleteBuffers, 1, &NormalBuffer);
	CHECK_GL(glDeleteProgram, ProgramID);
	CHECK_GL(glDeleteVertexArrays, 1, &VertexId);

	glfwTerminate();
#endif
	return 0;

}