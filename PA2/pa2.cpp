#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>

#include <cstdio>
#include  <cstdlib>

#include <GL/glew.h>

#include <glfw3.h>
#include <limits>


GLFWwindow *window;
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <algorithm>
#include <vector>
#ifdef _MSC_VER

typedef __int16 int16;
typedef unsigned __int16 uint16;
typedef __int32 int32;
typedef unsigned __int32 uint32;
typedef __int8 int8;
typedef unsigned __int8 uint8;

typedef unsigned __int64 uint64;
typedef __int64 int64;

#include <tchar.h>
typedef TCHAR CHAR;
#endif


void CheckOpenGLError(const char* stmt, const char* fname, int line)
{
	GLenum err = glGetError();
	if (err != GL_NO_ERROR)
	{
		printf("OpenGL error %08x, at %s:%i - for %s\n", err, fname, line, stmt);
		abort();
	}
}

#ifdef _DEBUG
#define GL_CHECK(stmt) do { \
	stmt; \
	CheckOpenGLError(#stmt, __FILE__, __LINE__); \
} while (0)
#else
#define GL_CHECK(stmt) stmt
#endif

//#define PHONG_SHADING
//#define NO_SHADING
#define GOURAUD_SHADING
//#define FLAT_SHADING

int     gNumVertices = 0;    // Number of 3D vertices.
int     gNumTriangles = 0;    // Number of triangles.
int*    gIndexBuffer = NULL; // Vertex indices for the triangles.


std::vector<glm::vec4> gVertices;
std::vector<glm::vec4> gScreenPos;
std::vector<glm::vec3> gColor;
std::vector<float> gDepth;

glm::mat4 vScaling;
glm::mat4 vModelTrans;
glm::mat4 vModel;

glm::mat4 vCamera;
glm::mat4 vPerspective;

glm::mat4 vViewPort;
const uint32 vNx = 512;
const uint32 vNy = 512;

float* vPixelData;
float* vDepth;

glm::vec3 vLight(-4, 4, -3);
glm::vec4 screeneye;
glm::vec4 screenLight;

glm::vec4 vCenter(0, 0, 0, 1);
std::vector<glm::vec3> vNormals;
glm::vec3 vKa(0.0f, 1.0f, 0.0f), vKd(0.0f, 0.5f, 0.0f), vKs(0.5f, 0.5f, 0.5f);
uint32 vP = 32;
float vIa = 0.2;
std::vector<glm::vec4> gTSVertices; 

void create_scene()
{
	int width = 32;
	int height = 16;

	float theta, phi;
	int t;

	gNumVertices = (height - 2) * width + 2;
	gNumTriangles = (height - 2) * (width - 1) * 2;

	// TODO: Allocate an array for gNumVertices vertices.
    glm::vec4 vertex;    
	gIndexBuffer = new int[3 * gNumTriangles];

	t = 0;
	for (int j = 1; j < height - 1; ++j)
	{
		for (int i = 0; i < width; ++i)
		{
			theta = (float)j / (height - 1) * M_PI;
			phi = (float)i / (width - 1)  * M_PI * 2;

			float   x = sinf(theta) * cosf(phi);
			float   y = cosf(theta);
			float   z = -sinf(theta) * sinf(phi);

			// TODO: Set vertex t in the vertex array to {x, y, z}.
            vertex.x = x; vertex.y = y; vertex.z = z; vertex.w = 1;
            gVertices.push_back(vertex);
			t++;
		}
	}

	// TODO: Set vertex t in the vertex array to {0, 1, 0}.
    vertex.x = 0; vertex.y = 0; vertex.z = 0; vertex.w = 1;
    gVertices.push_back(vertex);
	t++;

	// TODO: Set vertex t in the vertex array to {0, -1, 0}.
    vertex.x = 0; vertex.y = -1; vertex.z = 0; vertex.w = 1;
    gVertices.push_back(vertex);


	t++;

	t = 0;
	for (int j = 0; j < height - 3; ++j)
	{
		for (int i = 0; i < width - 1; ++i)
		{
			gIndexBuffer[t++] = j*width + i;
			gIndexBuffer[t++] = (j + 1)*width + (i + 1);
			gIndexBuffer[t++] = j*width + (i + 1);
			gIndexBuffer[t++] = j*width + i;
			gIndexBuffer[t++] = (j + 1)*width + i;
			gIndexBuffer[t++] = (j + 1)*width + (i + 1);
		}

	}
	for (int i = 0; i < width - 1; ++i)
	{
		gIndexBuffer[t++] = (height - 2)*width;
		gIndexBuffer[t++] = i;
		gIndexBuffer[t++] = i + 1;
		gIndexBuffer[t++] = (height - 2)*width + 1;
		gIndexBuffer[t++] = (height - 3)*width + (i + 1);
		gIndexBuffer[t++] = (height - 3)*width + i;
	}

	// The index buffer has now been generated. Here's how to use to determine
	// the vertices of a triangle. Suppose you want to determine the vertices
	// of triangle i, with 0 <= i < gNumTriangles. Define:
	//
	// k0 = gIndexBuffer[3*i + 0]
	// k1 = gIndexBuffer[3*i + 1]
	// k2 = gIndexBuffer[3*i + 2]
	//
	// Now, the vertices of triangle i are at positions k0, k1, and k2 (in that
	// order) in the vertex array (which you should allocate yourself at line
	// 27).
	//
	// Note that this assumes 0-based indexing of arrays (as used in C/C++,
	// Java, etc.) If your language uses 1-based indexing, you will have to
	// add 1 to k0, k1, and k2.
}

// Compute barycentric coordinates (u, v, w) for
// point p with respect to triangle (a, b, c)

// Borrowed from some website, to compute the area of triangle using cross product
bool Barycentric(glm::vec2 p, glm::vec2 a, glm::vec2 b, glm::vec2 c, float &u, float &v, float &w)
{
    glm::vec2 v0 = b - a, v1 = c - a, v2 = p - a;
    
    float d00 = glm::dot(v0, v0);
    float d01 = glm::dot(v0, v1);
    float d11 = glm::dot(v1, v1);
    float d20 = glm::dot(v2, v0);
    float d21 = glm::dot(v2, v1);
    float denom = d00 * d11 - d01 * d01;
    v = (d11 * d20 - d01 * d21) / denom;
    w = (d00 * d21 - d01 * d20) / denom;
    u = 1.0f - v - w;
    if(v >= 0 && w >= 0 && u>=0)
        return true;
    else return false;

}


void create_matrices(){

    vScaling = glm::mat4(2.0f, 0.0f, 0.0f, 0.0f,   
                         0.0, 2.0, 0.0, 0.0,
                         0.0f, 0.0f, 2.0f, 0.0f,  
                         0.0f, 0.0f, 0.0f, 1.0f);
    vScaling = glm::transpose(vScaling);

    vModelTrans = glm::mat4(1.0f, 0.0f, 0.0f, 0.0f, 
                            0.0f, 1.0f, 0.0f, 0.0f,
                            0.0f, 0.0f, 1.0f, -7.0f, 
                            0.0f, 0.0f, 0.0f, 1.0f);

    vModelTrans = glm::transpose(vModelTrans);


    vCamera = glm::mat4(1.0f, 0.0f, 0.0f, 0.0f, 
                        0.0f, 1.0f, 0.0f, 0.0f,
                        0.0f, 0.0f, 1.0f, 0.0f,   
                        0.0f, 0.0f, 0.0f, 1.0f);

    vCamera = glm::transpose(vCamera);


    float r = 0.1f, l = -0.1, b = -0.1, t = 0.1, n = -0.1, f = -1000.0;

    vPerspective = glm::mat4( (2*n)/(r-l), 0.0f, (l+r)/(l-r), 0.0f,
                             0.0f, (2*n)/(t-b), (b+t)/(b-t), 0.0f,
                             0.0f, 0.0f, (n+f)/(n-f), (2*f*n)/(f-n),
                             0.0f, 0.0f, 1.0f, 0.0f
                            );

    vPerspective = glm::transpose(vPerspective);


    vViewPort = glm::mat4((1.0 * vNx)/2, 0.0f, 0.0f, (1.0*vNx -1)/2,
                          0.0f, (1.0*vNy)/2, 0.0f, (1.0*vNy - 1)/2,
                          0.0f, 0.0f, 1.0f, 0.0f,
                          0.0f, 0.0f, 0.0f, 1.0f
                          );

    vViewPort = glm::transpose(vViewPort);

}
  
void FlatVertexShader(){

 std::vector<glm::vec4> gTSVertices; 
    for(int i = 0; i < gVertices.size(); i++){
        glm::vec4 temp = (vViewPort* vPerspective* vCamera* vModelTrans* vScaling) * gVertices[i];
        
        temp = temp/temp.w;
        gScreenPos.push_back(temp);

        temp = vModelTrans * vScaling *gVertices[i];
        temp = temp/temp.w;
        gTSVertices.push_back(temp);
    }
    glm::vec4 TransCenter = vModelTrans * vScaling * vCenter;
    glm::vec3 Fcenter(TransCenter.x/TransCenter.w, TransCenter.y/TransCenter.w, TransCenter.z/TransCenter.w);

    for(int i = 0; i < gNumTriangles; i++) {

        uint32 k0 = gIndexBuffer[3*i + 0];
        uint32 k1 = gIndexBuffer[3*i + 1];
        uint32 k2 = gIndexBuffer[3*i + 2];

        glm::vec3 v1 = glm::vec3(gTSVertices[k0]);
        glm::vec3 v2 = glm::vec3(gTSVertices[k1]);
        glm::vec3 v3 = glm::vec3(gTSVertices[k2]);

        glm::vec3 normal = glm::normalize(glm::cross(v2 - v1 , v3 - v1));
        vNormals.push_back(normal);      

        glm::vec3 centroid = glm::vec3( (v1.x + v2.x + v3.x)/3, (v1.y + v2.y + v3.y)/3, (v1.z + v2.z + v3.z)/3 );
        glm::vec3 l = glm::normalize(vLight - centroid);
	    glm::vec3 v = glm::normalize(glm::vec3(0.0, 0.0, 0.0) - centroid);	
        glm::vec3 h = glm::normalize(v) + glm::normalize(l);
	    h = glm::normalize(h); 
        glm::vec3 color;
	    glm::vec3 La = vKa * vIa;
		
	    glm::vec3 Ld = vKd * std::max((float)0.0, glm::dot(vNormals[i],l));

        glm::vec3 Ls = vKs * std::max((float)0.0, std::powf(glm::dot(vNormals[i],h),vP));
	    color = La + Ld + Ls;
	    gColor.push_back(color);	


    }

}

void vertexShader(){

    for(int i = 0; i < gVertices.size(); i++){
        glm::vec4 temp = (vViewPort* vPerspective* vCamera* vModelTrans* vScaling) * gVertices[i];
        
        temp = temp/temp.w;
        gScreenPos.push_back(temp);
        temp = vModelTrans * vScaling *gVertices[i];
        temp = temp/temp.w;
        gTSVertices.push_back(temp);
    }
    glm::vec4 TransCenter = vModelTrans * vScaling * vCenter;
    glm::vec3 Fcenter(TransCenter.x/TransCenter.w, TransCenter.y/TransCenter.w, TransCenter.z/TransCenter.w);
    screenLight =  (vViewPort* vPerspective* vCamera* vModelTrans* vScaling) * glm::vec4(vLight, 1.0);
    screeneye = (vViewPort* vPerspective* vCamera* vModelTrans* vScaling) * glm::vec4(0.0, 0.0, 0.0, 1.0);
    //depth computed already 

    // compute color for all the verices
    for(int i = 0; i < gVertices.size(); i++){        
        glm::vec3 normal = glm::normalize( -Fcenter + glm::vec3(gTSVertices[i].x, gTSVertices[i].y, gTSVertices[i].z));                 
		vNormals.push_back(normal);        
    }

#ifdef NO_SHADING
    for(int i = 0; i<gVertices.size(); i++){

        glm::vec3 color(1.0f, 1.0f, 1.0f);
        gColor.push_back(color);
        
    }
#endif



#ifdef GOURAUD_SHADING

  for(int i=0; i<gVertices.size(); i++){

	glm::vec3 l = glm::normalize(vLight - glm::vec3(gTSVertices[i].x, gTSVertices[i].y, gTSVertices[i].z));
	glm::vec3 v = glm::normalize(glm::vec3(0.0, 0.0, 0.0) -  glm::vec3(gTSVertices[i].x, gTSVertices[i].y, gTSVertices[i].z));
    glm::vec3 h = glm::normalize(v) + glm::normalize(l);
	h = glm::normalize(h); 

    glm::vec3 color;
	glm::vec3 La = vKa * vIa;
		
	glm::vec3 Ld = vKd * std::max((float)0.0, glm::dot(vNormals[i],l));

    glm::vec3 Ls = vKs * std::max((float)0.0, std::powf(glm::dot(vNormals[i],h),vP)); 
	color = La  + Ld + Ls;
	gColor.push_back(color);	

  }

#endif



#ifdef PHONG_SHADING

  for(int i=0; i<gVertices.size(); i++){

	glm::vec3 l = glm::normalize(vLight - glm::vec3(gTSVertices[i].x, gTSVertices[i].y, gTSVertices[i].z));
	glm::vec3 v = glm::normalize(glm::vec3(0.0, 0.0, 0.0) -  glm::vec3(gTSVertices[i].x, gTSVertices[i].y, gTSVertices[i].z));
    glm::vec3 h = glm::normalize(v) + glm::normalize(l);
	h = glm::normalize(h); 

    glm::vec3 color;
	glm::vec3 La = vKa * vIa;
		
	glm::vec3 Ld = vKd * std::max((float)0.0, glm::dot(vNormals[i],l));

    glm::vec3 Ls = vKs * std::max((float)0.0, std::powf(glm::dot(vNormals[i],h),vP)); 
	color = La  + Ld + Ls;
	gColor.push_back(color);	

  }

#endif
    // different shaders, compute normals

}

void FlatFragmentShader(){

	for(int i = 0; i < gNumTriangles; i++){


	  uint32 k0 = gIndexBuffer[3*i + 0];
	  uint32 k1 = gIndexBuffer[3*i + 1];
	  uint32 k2 = gIndexBuffer[3*i + 2];


	  glm::vec4 v1 = gScreenPos[k0];
	  glm::vec4 v2 = gScreenPos[k1];
	  glm::vec4 v3 = gScreenPos[k2];

	  int maxX = std::max(v1.x, std::max(v2.x, v3.x));
	  int minX = std::min(v1.x, std::min(v2.x, v3.x));
	  int maxY = std::max(v1.y, std::max(v2.y, v3.y));
	  int minY = std::min(v1.y, std::min(v2.y, v3.y));
	
	  glm::vec3 centroid = glm::vec3((v1.x + v2.x + v3.x)/3,(v1.y + v2.y + v3.y)/3,(v1.z + v2.z + v3.z)/3  );

      float u,v,w;
      bool isInside = Barycentric(glm::vec2(centroid.x,centroid.y), glm::vec2(v1.x, v1.y), glm::vec2(v2.x, v2.y), glm::vec2(v3.x, v3.y),
                            u, v, w);
      uint32 pixelOffset = 3 * (centroid.x * vNx + centroid.y);
	  glm::vec3 c = gColor[i];
               
	    for(int x = minX; x <= maxX; x++){

            for(int y = minY; y <= maxY; y++){

                

                float u,v,w;
                bool isInside = Barycentric(glm::vec2(x,y), glm::vec2(v1.x, v1.y), glm::vec2(v2.x, v2.y), glm::vec2(v3.x, v3.y),
                            u, v, w);
               float depth = std::abs(u*v1.z + v*v2.z + w*v3.z); 
                uint32 pixelOffset = 3 * ((y) * vNx + (x));
                if(isInside){            
                   if(depth < vDepth[(y) * vNx + (x)]){
                    
                        vDepth[( y) * vNx + (x)] = depth;
			            vPixelData[pixelOffset + 0] = std::powf(c.r, 1/2.2);
                        vPixelData[pixelOffset + 1] = std::powf(c.g, 1/2.2);
                        vPixelData[pixelOffset + 2] = std::powf(c.b, 1/2.2);

                    }                 
                }
            
            }
        }
	  

    }

}

void gourardFragmentShader(){

    for(int i=0; i<gScreenPos.size(); i++){

        int x = gScreenPos[i].x;
        int y = gScreenPos[i].y;
        uint32 pixelOffset = 3*(x * vNx + y);
        vPixelData[pixelOffset + 0] = std::powf( gColor[i].r, 1/2.2);
        vPixelData[pixelOffset + 1] = std::powf(gColor[i].g, 1/2.2);
        vPixelData[pixelOffset + 2] = std::powf(gColor[i].b, 1/2.2);        

    }

for(int i = 0; i < gNumTriangles; i++){


  uint32 k0 = gIndexBuffer[3*i + 0];
  uint32 k1 = gIndexBuffer[3*i + 1];
  uint32 k2 = gIndexBuffer[3*i + 2];


  glm::vec4 v1 = gScreenPos[k0];
  glm::vec4 v2 = gScreenPos[k1];
  glm::vec4 v3 = gScreenPos[k2];

  int maxX = std::max(v1.x, std::max(v2.x, v3.x));
  int minX = std::min(v1.x, std::min(v2.x, v3.x));
  int maxY = std::max(v1.y, std::max(v2.y, v3.y));
  int minY = std::min(v1.y, std::min(v2.y, v3.y));

    
  for(int y = minY; y <= maxY; y++){

    glm::vec2 first, second;
    glm::vec3 firstColor, secondColor;
    bool isFirst = false;
    for(int x = minX; x <= maxX; x++){
      float u,v,w;
      uint32 pixelOffset = 3 * (x * vNx + y); 
      bool isInside = Barycentric(glm::vec2(x,y), glm::vec2(v1.x, v1.y), glm::vec2(v2.x, v2.y), glm::vec2(v3.x, v3.y),
                            u, v, w);       
     
      if(isInside && !isFirst){
            first = glm::vec2(x-1,y);
            isFirst = true;
            glm::vec3 c1 = gColor[k0];
            glm::vec3 c2 = gColor[k1];
            glm::vec3 c3 = gColor[k2];
            firstColor = u*c1 + v*c2 + w*c3;
            float depth = u*v1.z + v*v2.z + w*v3.z; 
                if(depth <= vDepth[x * vNx + y]){
                    
                    vDepth[x * vNx + y] = depth;
			        vPixelData[pixelOffset + 0] = std::powf(firstColor.r, 1/2.2);
                    vPixelData[pixelOffset + 1] = std::powf(firstColor.g, 1/2.2);
                    vPixelData[pixelOffset + 2] = std::powf(firstColor.b, 1/2.2);
                } 
       }

       if(!isInside && isFirst){
            second = glm::vec2(x, y);
            glm::vec3 c1 = gColor[k0];
            glm::vec3 c2 = gColor[k1];
            glm::vec3 c3 = gColor[k2];
            secondColor = u*c1 + v*c2 + w*c3;
            float depth = u*v1.z + v*v2.z + w*v3.z; 
                if(depth <= vDepth[x * vNx + y]){
                    
                    vDepth[x * vNx + y] = depth;
			        vPixelData[pixelOffset + 0] = std::powf(secondColor.r, 1/2.2);
                    vPixelData[pixelOffset + 1] = std::powf(secondColor.g, 1/2.2);
                    vPixelData[pixelOffset + 2] = std::powf(secondColor.b, 1/2.2);
                }  
            break; 
        }

    }
    
    for(int x =  first.x; x <= second.x; x++){
            uint32 pixelOffset = 3 * (x * vNx + y);  
            float u, v, w;
            bool isInside = Barycentric(glm::vec2(x,y), glm::vec2(v1.x, v1.y), glm::vec2(v2.x, v2.y), glm::vec2(v3.x, v3.y),
                            u, v, w);  
            float depth = u*v1.z + v*v2.z + w*v3.z; 
                if(depth <= vDepth[x * vNx + y]){
                    
                    vDepth[x * vNx + y] = depth;
                    glm::vec3 c = ((x-first.x)/(second.x - first.x)) * firstColor + ((second.x - x)/(second.x - first.x)) * secondColor;
			        vPixelData[pixelOffset + 0] = std::powf(c.r, 1/2.2);
                    vPixelData[pixelOffset + 1] = std::powf(c.g, 1/2.2);
                    vPixelData[pixelOffset + 2] = std::powf(c.b, 1/2.2);
                }           
    }

  } 

}




}

void phongFragmentShader() {

 for(int i = 0; i < gNumTriangles; i++){


    uint32 k0 = gIndexBuffer[3*i + 0];
    uint32 k1 = gIndexBuffer[3*i + 1];
    uint32 k2 = gIndexBuffer[3*i + 2];


    glm::vec4 v1 = gScreenPos[k0];
    glm::vec4 v2 = gScreenPos[k1];
    glm::vec4 v3 = gScreenPos[k2];

    int maxX = std::max(v1.x, std::max(v2.x, v3.x));
    int minX = std::min(v1.x, std::min(v2.x, v3.x));
    int maxY = std::max(v1.y, std::max(v2.y, v3.y));
    int minY = std::min(v1.y, std::min(v2.y, v3.y));

    for(int x = minX; x <= maxX; x++){
        for(int y = minY; y <= maxY; y++){

            float u,v,w;
            bool isInside = Barycentric(glm::vec2(x,y), glm::vec2(v1.x, v1.y), glm::vec2(v2.x, v2.y), glm::vec2(v3.x, v3.y),
                            u, v, w);
            uint32 pixelOffset = 3 * ((y) * vNx + x);
            if(isInside){
                glm::vec3 ver = u*glm::vec3(gTSVertices[k0]) + v*glm::vec3(gTSVertices[k1]) + w*glm::vec3(gTSVertices[k2]);
                glm::vec3 l = glm::normalize(vLight - ver);
	            glm::vec3 v_N = glm::normalize(glm::vec3(0.0, 0.0, 0.0) -  ver);
                glm::vec3 h = glm::normalize(v_N) + glm::normalize(l);
	            h = glm::normalize(h); 
                glm::vec3 N = u * vNormals[k0] + v*vNormals[k1] + w*vNormals[k2];
                glm::vec3 color;
	            glm::vec3 La = vKa * vIa;
		
	            glm::vec3 Ld = vKd * std::max((float)0.0, glm::dot(N,l));

                glm::vec3 Ls = vKs * std::max((float)0.0, std::powf(glm::dot(N,h),vP)); 
	            color = La  + Ld + Ls;
	           	
                glm::vec3 c1 = gColor[k0];
                glm::vec3 c2 = gColor[k1];
                glm::vec3 c3 = gColor[k2];
            
                glm::vec3 c = u*c1 + v*c2 + w*c3;
                float depth = std::abs(u*  v1.z + v*v2.z + w*v3.z);
                if(depth < vDepth[(y) * vNx +  x]){
                    
                    vDepth[(y) * vNx + x] = depth;
			        vPixelData[pixelOffset + 0] = std::powf(color.r, 1/2.2);
                    vPixelData[pixelOffset + 1] = std::powf(color.g, 1/2.2);
                    vPixelData[pixelOffset + 2] =  std::powf(color.b, 1/2.2);
                }
                 
            }
            
        }
    } 
    
    
  }


}

void fragmentShader(){

// loop over all triangles 

  for(int i = 0; i < gNumTriangles; i++){


    uint32 k0 = gIndexBuffer[3*i + 0];
    uint32 k1 = gIndexBuffer[3*i + 1];
    uint32 k2 = gIndexBuffer[3*i + 2];


    glm::vec4 v1 = gScreenPos[k0];
    glm::vec4 v2 = gScreenPos[k1];
    glm::vec4 v3 = gScreenPos[k2];

    int maxX = std::max(v1.x, std::max(v2.x, v3.x));
    int minX = std::min(v1.x, std::min(v2.x, v3.x));
    int maxY = std::max(v1.y, std::max(v2.y, v3.y));
    int minY = std::min(v1.y, std::min(v2.y, v3.y));

    for(int x = minX; x <= maxX; x++){
        for(int y = minY; y <= maxY; y++){

            float u,v,w;
            bool isInside = Barycentric(glm::vec2(x,y), glm::vec2(v1.x, v1.y), glm::vec2(v2.x, v2.y), glm::vec2(v3.x, v3.y),
                            u, v, w);
            uint32 pixelOffset = 3 * ((y) * vNx + x);
            if(isInside){
                glm::vec3 c1 = gColor[k0];
                glm::vec3 c2 = gColor[k1];
                glm::vec3 c3 = gColor[k2];
            
                glm::vec3 c = u*c1 + v*c2 + w*c3;
                float depth = std::abs( u*v1.z + v*v2.z + w*v3.z );
                if(depth < vDepth[(y) * vNx +  x]){
                    
                    vDepth[(y) * vNx + x] = depth;
			        vPixelData[pixelOffset + 0] = std::powf(c.r, 1/2.2);
                    vPixelData[pixelOffset + 1] = std::powf(c.g, 1/2.2);
                    vPixelData[pixelOffset + 2] =  std::powf(c.b, 1/2.2);
                }
                 
            }
            
        }
    } 
    
    
  }



}

int main(int argc, char *argv[]){


  vPixelData = (float*)malloc(3*vNx*vNy*sizeof(float));
  vDepth = (float*)malloc(vNx * vNy * sizeof(float));
  memset((vPixelData), 0.0, 3 * vNx*vNy);
  for(int i = 0; i < vNx*vNy; i++)
    vDepth[i] = std::numeric_limits<float>::max();
  create_scene();
  create_matrices();
  //vertexShader();
#ifdef NO_SHADING
  vertexShader();
  fragmentShader();
 
#endif

#ifdef FLAT_SHADING
    FlatVertexShader();
	FlatFragmentShader();
#endif 

#ifdef GOURAUD_SHADING
    vertexShader();
    fragmentShader();
	//gourardFragmentShader();
#endif

#ifdef PHONG_SHADING
    vertexShader();
	phongFragmentShader();
#endif




  if (!glfwInit()){

		fprintf(stderr, "Failed to initialze GLFW\n");
		getchar();
		return -1;
	}
	
	
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	window = glfwCreateWindow(vNx,vNy,"PA1", NULL,NULL);
	if (window == NULL){
		fprintf(stderr, "Failed to initialize window");
		getchar();
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);


	glewExperimental = true;
	if (glewInit() != GLEW_OK){
		fprintf(stderr, "Failed to initial OpenGL glew");
		getchar();
		glfwTerminate();
		return -1;
	}

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	
	//
	do{
		glClear(GL_COLOR_BUFFER_BIT);
		

		//draw pixels here
		glDrawPixels(
			vNx,
			vNy,
			GL_RGB,
			GL_FLOAT,
			vPixelData
			);
		//swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	}  while (glfwGetKey(window,  GLFW_KEY_ESCAPE) != GLFW_PRESS &&
		glfwWindowShouldClose(window) == 0);

	glfwTerminate();

return 0;
}

