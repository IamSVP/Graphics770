#include <cstdio>
#include  <cstdlib>

#include <GL/glew.h>

#include <glfw3.h>

GLFWwindow *window;
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <algorithm>
#include <vector>
#ifdef _MSC_VER

typedef __int16 int16;
typedef unsigned __int16 uint16;
typedef __int32 int32;
typedef unsigned __int32 uint32;
typedef __int8 int8;
typedef unsigned __int8 uint8;

typedef unsigned __int64 uint64;
typedef __int64 int64;

#include <tchar.h>
typedef TCHAR CHAR;
#endif


void CheckOpenGLError(const char* stmt, const char* fname, int line)
{
	GLenum err = glGetError();
	if (err != GL_NO_ERROR)
	{
		printf("OpenGL error %08x, at %s:%i - for %s\n", err, fname, line, stmt);
		abort();
	}
}

#ifdef _DEBUG
#define GL_CHECK(stmt) do { \
	stmt; \
	CheckOpenGLError(#stmt, __FILE__, __LINE__); \
} while (0)
#else
#define GL_CHECK(stmt) stmt
#endif

typedef struct _sphere {

	_sphere(glm::vec3 _center, uint32 _radius) :
	center(_center), radius(_radius){}

	_sphere() :
	center(glm::vec3(0)), radius(0){}

	_sphere(glm::vec3 _center, uint32 _radius, glm::vec3 _ka, glm::vec3 _kd, glm::vec3 _ks, uint32 _specularPower) :
		center(_center),
		radius(_radius),
		ka(_ka),
		kd(_kd),
		ks(_ks),
		specularPower(_specularPower){}

	glm::vec3 center;
	uint32 radius;
	glm::vec3 ka, kd, ks;
	uint32 specularPower;
} sphere;

typedef struct _plane{

	_plane() :
	normal(glm::vec3(0)), point0(0){}

	_plane(glm::vec3 _normal, glm::vec3 _point, glm::vec3 _ka, glm::vec3 _kd, glm::vec3 _ks, uint32 _specularPower) :
		normal(_normal), 
		point0(_point),
		ka(_ka),
		kd(_kd),
		ks(_ks),
		specularPower(_specularPower)
		{}

	glm::vec3 normal;
	glm::vec3 point0;
	glm::vec3 ka, kd, ks;
	uint32 specularPower;
} plane;



// define kind of rendering you want 
// #deinfe RAY_TRACE  //just to do raytracking without any kind of shading
 #define PHONG_SHADING // to render using Phong shading without Anti Aliasing
// #define ANTI_ALIASING

//#define ANTI_ALIASING






//global values
float* vPixelData;
const uint32 vNx = 512;
const uint32 vNy = 512;
sphere s1;
sphere s2;
sphere s3;
plane p1;
glm::vec3 light(-4, 4, -3);

void computeAllIntersections(
	glm::vec3 p,
	glm::vec3 d_ray,
	std::vector<float> &intersectionPoints,
	std::vector<int> & indexToObjects
	)
{

	float dDot = glm::dot(d_ray, d_ray);
	float pDot = glm::dot(p - s1.center, p - s1.center);
	float dpDot = glm::dot(p - s1.center, d_ray);
	float temp = (dpDot * dpDot) - (dDot*(pDot - (s1.radius*s1.radius)));
	if (temp > 0){

		float t1 = (-dpDot + sqrt(temp)) / dDot;
		float t2 = (-dpDot - sqrt(temp)) / dDot;

		if (t1 > 0)
		{
			intersectionPoints.push_back(t1);
			indexToObjects.push_back(1);
		}
		if (t2 > 0) {
			intersectionPoints.push_back(t2);
			indexToObjects.push_back(1);
		}

	}

	//with second shpere
	// radius ==2
	dDot = glm::dot(d_ray, d_ray);
	pDot = glm::dot(p - s2.center, p - s2.center);
	dpDot = glm::dot(p - s2.center, d_ray);
	temp = (dpDot * dpDot) - (dDot*(pDot - (s2.radius*s2.radius)));
	if (temp > 0){

		float t1 = (-dpDot + sqrt(temp)) / dDot;
		float t2 = (-dpDot - sqrt(temp)) / dDot;

		if (t1 > 0)
		{
			intersectionPoints.push_back(t1);
			indexToObjects.push_back(2);
		}
		if (t2 > 0) {
			intersectionPoints.push_back(t2);
			indexToObjects.push_back(2);
		}
	}


	//sphere 3
	dDot = glm::dot(d_ray, d_ray);
	pDot = glm::dot(p - s3.center, p - s3.center);
	dpDot = glm::dot(p - s3.center, d_ray);
	temp = (dpDot * dpDot) - (dDot*(pDot - (s3.radius*s3.radius)));
	if (temp > 0){

		float t1 = (-dpDot + sqrt(temp)) / dDot;
		float t2 = (-dpDot - sqrt(temp)) / dDot;

		if (t1 > 0)
		{
			intersectionPoints.push_back(t1);
			indexToObjects.push_back(3);
		}
		if (t2 > 0) {
			intersectionPoints.push_back(t2);
			indexToObjects.push_back(3);
		}
	}

	// plane


	float pointNDot = glm::dot(p1.point0, p1.normal);
	float pNDot = glm::dot(p, p1.normal);
	float dNDot = glm::dot(d_ray, p1.normal);
	float t_temp = (pointNDot - pNDot) / dNDot;
	if (t_temp > 0){
		intersectionPoints.push_back(t_temp);
		indexToObjects.push_back(4);
	}



}



void getMinimumDistanceObject(std::vector<float> &intersectionPoints,
	std::vector<int> &indexToObjects,
	glm::vec3 d_ray,
	glm::vec3 p,
	float &minDistance,
	uint32 &objIndex,
	glm::vec3 &intersectionPoint
	)
{

	if (!intersectionPoints.empty()){

		glm::vec3 distanceVector(p + intersectionPoints[0] * d_ray);
		minDistance = glm::length(distanceVector);
		objIndex = indexToObjects[0];
		intersectionPoint = distanceVector;
		for (uint32 i = 1; i<intersectionPoints.size(); i++){

			distanceVector = glm::vec3(p + intersectionPoints[i] * d_ray);
			uint32 temp_dist = glm::length(distanceVector);
			if (temp_dist < minDistance){
				minDistance = temp_dist;
				objIndex = indexToObjects[i];
				intersectionPoint = distanceVector;
			}

		}

	}

}




// shading for sphere
glm::vec3 computeColorsphere(
	sphere s, //shpere
	uint32 pixOffset, // pixeloffset
	glm::vec3 point, // intersection point
	glm::vec3 light, // light postion
	glm::vec3 eyepos,// eyepositon
	uint32 objNumber	
	)

{

	glm::vec3 color(1.0f, 1.0f, 1.0f);

	glm::vec3 normal = glm::normalize(s.center - point);
	glm::vec3 l = glm::normalize(point - light);
	glm::vec3 v = glm::normalize(point - eyepos);
	// Ld 
	glm::vec3 Ld = s.kd * std::max((float)0.0, glm::dot(normal, l));


	glm::vec3 h = glm::normalize(v) + glm::normalize(l);
	h = glm::normalize(h);

	// Ls
	glm::vec3 Ls = s.ks * std::max(float(0.0), std::powf(glm::dot(normal,h),s.specularPower));

	//La ambient 
	glm::vec3 La = s.ka;

	std::vector<float> intersectionPoints;
	std::vector<int> indexToObjects;
	glm::vec3 d_ray = glm::normalize(l);
	computeAllIntersections(light, d_ray, intersectionPoints, indexToObjects);


	float minDistance;
	uint32 objIndex = 0;
	glm::vec3 intersectionPoint;


	getMinimumDistanceObject(
		intersectionPoints,
		indexToObjects,
		d_ray,
		light,
		minDistance,
		objIndex,
		intersectionPoint
		);

	if (objIndex == objNumber)
		color = La + Ld + Ls;
	else
		color = La;

	color.r = std::powf(color.r, 1/2.2);
	color.g = std::powf(color.g, 1/2.2);
	color.b = std::powf(color.b, 1/2.2);
	return color;
	
}



// shading for plane
glm::vec3 computeColorPlane(
	plane p,
	uint32 pixOffset,
	glm::vec3 point,
	glm::vec3 light,
	glm::vec3 eyepos,
	uint32 objNumber
	)
{
	glm::vec3 color(1.0f, 1.0f, 1.0f);

	glm::vec3 normal = glm::normalize(p.normal);
	glm::vec3 l = glm::normalize(light - point);
	glm::vec3 v = glm::normalize(point - eyepos);
	// Ld 
	glm::vec3 Ld = p.kd * std::max((float)0.0, glm::dot(normal, l));



	glm::vec3 h = glm::normalize(v) + glm::normalize(l);
	h = glm::normalize(h);



	// Ls
	glm::vec3 Ls = p.ks * std::max(float(0.0), std::powf(glm::dot(normal, h), p.specularPower));

	//La ambient 
	glm::vec3 La = p.ka;
	std::vector<float> intersectionPoints;
	std::vector<int> indexToObjects;
	glm::vec3 d_ray = glm::normalize(-l);
	computeAllIntersections(light, d_ray, intersectionPoints, indexToObjects);


	float minDistance;
	uint32 objIndex = 0;
	glm::vec3 intersectionPoint;


	getMinimumDistanceObject(
		intersectionPoints,
		indexToObjects,
		d_ray,
		light,
		minDistance,
		objIndex,
		intersectionPoint
		);

	if (objIndex == objNumber)
		color = La + Ld + Ls;
	else
		color = La;
	color.r = std::powf(color.r, 1/2.2);
	color.g = std::powf(color.g, 1/2.2);
	color.b = std::powf(color.b, 1/2.2);
	return color;
	
}


void RayTrace(float screen_u, float screen_v,float d,glm::vec3 u, glm::vec3 v, glm::vec3 w, uint32 pixelOffset,glm::vec3 p){
	glm::vec3 d_ray;
	d_ray = screen_u *u + screen_v *v - d * w; //check what it returns
	d_ray = glm::normalize(d_ray);
	std::vector<float> intersectionPoints;
	std::vector<int> indexToObjects;


	// call the intersections 

	computeAllIntersections(p, d_ray, intersectionPoints, indexToObjects);
	// with first sphere 
	// unit radius == 1



	if (!intersectionPoints.empty()){
		for (int k = 0; k < 3; k++)
			vPixelData[pixelOffset + k] = 1.0;
	}


}

glm::vec3 PhongShading(float screen_u, float screen_v, float d, glm::vec3 u, glm::vec3 v, glm::vec3 w,uint32 pixelOffset,glm::vec3 p){


	glm::vec3 d_ray;
	d_ray = screen_u *u + screen_v *v - d * w; //check what it returns
	d_ray = glm::normalize(d_ray);
	std::vector<float> intersectionPoints;
	std::vector<int> indexToObjects;


	// call the intersections 

	computeAllIntersections(p, d_ray, intersectionPoints, indexToObjects);
	// with first sphere 
	// unit radius == 1



	//if (!intersectionPoints.empty()){
	//	for (int k = 0; k < 3; k++)
	//		vPixelData[pixelOffset + k] = 1.0;
	//}



	float minDistance;
	uint32 objIndex = 0;
	glm::vec3 intersectionPoint;


	getMinimumDistanceObject(
		intersectionPoints,
		indexToObjects,
		d_ray,
		p,
		minDistance,
		objIndex,
		intersectionPoint
		);
	glm::vec3 color;
	glm::vec3 fullColor(255, 255, 255);
	switch (objIndex){


	case 1:

		color = computeColorsphere(s1, pixelOffset, intersectionPoint, light, p, 1);
		break;
	case 2:
		color = computeColorsphere(s2, pixelOffset, intersectionPoint, light, p, 2);
		break;
	case 3:
		return computeColorsphere(s3, pixelOffset, intersectionPoint, light, p, 3);
		break;
	case 4:
		return computeColorPlane(p1, pixelOffset, intersectionPoint, light, p, 4);
		break;
	default:
		return glm::vec3(0.0f, 0.0f, 0.0f);
		break;

	}

}




void FillColorBuffer(){

	glm::vec3 c(-4.0f, 0.0f, -7.0f);
	uint32 rd = 1;
	glm::vec3 ka;
	glm::vec3 kd;
	glm::vec3 ks;
	
	uint32 specularPower;
	

	ka = glm::vec3(0.2f,0.0f,0.0f);
	kd = glm::vec3(1.0f,0.0f,0.0f);
	ks = glm::vec3(0.0f,0.0f,0.0f);
	specularPower = 0;
	s1 = sphere(c,rd,ka,kd,ks,specularPower);

	c.x = 0.0f; c.y = 0.0f; c.z = -7.0f;
	rd= 2;
	ka = glm::vec3(0.0f,0.2f,0.0f);
	kd = glm::vec3(0.0f, 0.5f, 0.0f);
	ks = glm::vec3(0.5f, 0.5f, 0.5f);
	specularPower = 32;
	s2 = sphere(c, rd, ka, kd, ks, specularPower);

	c.x = 4.0f; c.y = 0.0f; c.z = -7.0f;
	rd = 1;

	ka = glm::vec3(0.0f, 0.0f, 0.2f);
	kd = glm::vec3(0.0f, 0.0f, 1.0f);
	ks = glm::vec3(0.0f, 0.0f, 0.0f);
	specularPower = 0;
	s3 = sphere(c,rd, ka, kd, ks, specularPower);


	glm::vec3 normal(0.0f, 1.0f, 0.0f);
	glm::vec3 pointOnPlane(0.0f, -2.0f, 0.0f);
	ka = glm::vec3(0.2f, 0.2f, 0.2f);
	kd = glm::vec3(1.0f,1.0f,1.0f);
	ks = glm::vec3(0.0f, 0.0f, 0.0f);
	specularPower = 0;
	p1 = plane(normal, pointOnPlane, ka, kd, ks, specularPower);


	//Define camera center and orientations
	glm::vec3 e(0.0f, 0.0f, 0.0f);
	glm::vec3 u(1.0f, 0.0f, 0.0f);
	glm::vec3 v(0.0f, 1.0f, 0.0f);
	glm::vec3 w(0.0f, 0.0f, 1.0f);

	float l = -0.1f;
	float r = 0.1f;
	float b = -0.1f;
	float t = 0.1f;
	float d = 0.1f;
	uint32 samples = 32;
	glm::vec3 p = glm::vec3(e);
	glm::vec3 d_ray(0.0f, 0.0f, 0.0f);
	for (uint32 py = 0; py < vNy; py++){
		for (uint32 px = 0; px < vNx; px++){

			uint32 pixelOffset = 3*(py *vNx + px);


			float screen_u = l + (((r-l) * (px + 0.5)) / vNx);
			float screen_v = b + (((t - b) * ( py + 0.5)) / vNy);
			glm::vec3 color;
			float hx = (r - l) / vNx;
			float hy = (t - b) / vNy;

#ifdef RAY_TRACE
			RayTrace(screen_u, screen_v, d,u,v,w,pixelOffset,p);
#endif

#ifdef PHONG_SHADING
            
			color = PhongShading(screen_u, screen_v, d, u, v, w, pixelOffset, p);
			vPixelData[pixelOffset + 0] = color.r;
			vPixelData[pixelOffset + 1] = color.g;
			vPixelData[pixelOffset + 2] = color.b;
#endif
			



#ifdef ANTI_ALIASING
			
			float px_l = l+ (px * hx);
			float px_r = l + ((px + 1) * hx);
			float py_b = b+ py * hy;
			float py_t = b +(py + 1) * hy;

			float px_h = (px_r - px_l) / 8;
			float py_h = (py_t - py_b) / 8;
			color = glm::vec3(0.0f, 0.0f, 0.0f);
			for (uint32 j = 0; j < 8; j++){
				for (uint32 i = 0; i < 8; i++){

					screen_u = px_l + i * px_h;
					screen_v = py_b + j * py_h;
					color += PhongShading(screen_u, screen_v, d, u, v, w, pixelOffset, p);
				}
			}
			color = (1.0f/64) * color;
			vPixelData[pixelOffset + 0] = color.r;
			vPixelData[pixelOffset + 1] = color.g;
			vPixelData[pixelOffset + 2] = color.b;
#endif

		}// end of pixel loop
	} // end of pixel loop
	

}

int main(int argc, char* argv[]){

	if (!glfwInit()){

		fprintf(stderr, "Failed to initialze GLFW\n");
		getchar();
		return -1;
	}
	
	
	//glfwWindowHint(GLFW_SAMPLES,4);
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,3);
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,3);
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	window = glfwCreateWindow(vNx,vNy,"PA1", NULL,NULL);
	if (window == NULL){
		fprintf(stderr, "Failed to initialize window");
		getchar();
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);


	glewExperimental = true;
	if (glewInit() != GLEW_OK){
		fprintf(stderr, "Failed to initial OpenGL glew");
		getchar();
		glfwTerminate();
		return -1;
	}

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	//glClearColor(0.0f, 0.0f, 0.5f, 0.0f);
	vPixelData = (float*)malloc(3*vNx*vNy*sizeof(float));
	//memset((vPixelData), 1.0, 3 * vNx*vNy);


	FillColorBuffer();
	//
	do{
		glClear(GL_COLOR_BUFFER_BIT);
		

		//draw pixels here
		glDrawPixels(
			vNx,
			vNy,
			GL_RGB,
			GL_FLOAT,
			vPixelData
			);
		//swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	}  while (glfwGetKey(window,  GLFW_KEY_ESCAPE) != GLFW_PRESS &&
		glfwWindowShouldClose(window) == 0);

	glfwTerminate();

	
}