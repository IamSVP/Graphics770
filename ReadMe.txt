Required things:
If checking in windows:
Visual Studio 2012 and above
Cmake.

If in linux:
Just cmake

Running in windows:
Created a folder with name build in the project
cd build
cmake ..
A vsiual studio project is built.
Open the solution, two projects named PA4 and PA_KdTree.

For part-I
You have to set PA4 as a start-up project and run it in only DEBUG mode! It crashes in release mode.


For part-II
You have to set PA_KdTree as a start-up project and run it in release mode.
In the source file PA_KdTree.cpp in line number 38 you'll find #define SHADOW, comment/un-comment to render without/with shadows.


Running in Linux:

same as above.
After cmake, do a make. The project folders will be build and you can find PA1 and do ./PA2 for running it.
